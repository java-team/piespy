#!/bin/bash

INI="$1"
shift
if [ "x$INI" == "x" ]; then
    INI="./config.ini"
fi

if [ ! -e "$INI" ]; then
    echo Configuration file "$INI" not found. >&2
    echo An example configuration file may be found in >&2
    echo /usr/share/doc/piespy/examples/config.ini . >&2
    exit 1
fi

if [ "x$2" != "x" ]; then
    echo "Usage: piespy [configuration file]" >&2
    exit 1
fi

exec java -Djava.awt.headless=true -classpath /usr/share/piespy/pircbot.jar:/usr/share/piespy/piespy.jar org.jibble.socnet.SocialNetworkBot "$INI"
